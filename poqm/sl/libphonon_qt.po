# Slovenian translation of libphonon
# Copyright (C) 2007 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marko Burjek <email4marko@gmail.com>, 2007.
# Jure Repinc <jlp@holodeck1.com>, 2008, 2009.
# Andrej Vernekar <andrej.vernekar@moj.net>, 2010, 2012.
# Andrej Mernik <andrejm@ubuntu.si>, 2013.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: libphonon\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-07-01 01:43+0200\n"
"PO-Revision-Date: 2020-01-23 16:27+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100>=3 && n"
"%100<=4 ? 2 : 3);\n"
"X-Generator: Poedit 2.2.4\n"
"X-Qt-Contexts: true\n"

#: audiooutput.cpp:481 audiooutput.cpp:519
#, qt-format
msgctxt "Phonon::AudioOutput|"
msgid ""
"<html>The audio playback device <b>%1</b> does not work.<br/>Falling back to "
"<b>%2</b>.</html>"
msgstr ""
"<html>Naprava za predvajanje zvoka <b>%1</b> ne deluje.<br/>Preklapljam "
"nazaj na <b>%2</b>.</html>"

#: audiooutput.cpp:494
#, qt-format
msgctxt "Phonon::AudioOutput|"
msgid ""
"<html>Switching to the audio playback device <b>%1</b><br/>which just became "
"available and has higher preference.</html>"
msgstr ""
"<html>Preklapljam na napravo za predvajanje zvoka <b>%1</b>,<br/>ki je "
"pravkar postala dostopna in ima višjo prednost.</html>"

#: audiooutput.cpp:497 audiooutput.cpp:515
#, qt-format
msgctxt "Phonon::AudioOutput|"
msgid "Revert back to device '%1'"
msgstr "Povrni nazaj na napravo '%1'"

#: audiooutput.cpp:512
#, qt-format
msgctxt "Phonon::AudioOutput|"
msgid ""
"<html>Switching to the audio playback device <b>%1</b><br/>which has higher "
"preference or is specifically configured for this stream.</html>"
msgstr ""
"<html>Preklapljam na napravo za predvajanje zvoka <b>%1</b>,<br/>ki ima "
"višjo prednost ali je posebej nastavljena za ta pretok.</html>"

#: mediacontroller.cpp:162
msgctxt "Phonon::MediaController|"
msgid "Main Menu"
msgstr "Glavni meni"

#: mediacontroller.cpp:164
msgctxt "Phonon::MediaController|"
msgid "Title Menu"
msgstr "Meni naslovov"

#: mediacontroller.cpp:166
msgctxt "Phonon::MediaController|"
msgid "Audio Menu"
msgstr "Meni zvoka"

#: mediacontroller.cpp:168
msgctxt "Phonon::MediaController|"
msgid "Subtitle Menu"
msgstr "Meni podnapisov"

#: mediacontroller.cpp:170
msgctxt "Phonon::MediaController|"
msgid "Chapter Menu"
msgstr "Meni poglavij"

#: mediacontroller.cpp:172
msgctxt "Phonon::MediaController|"
msgid "Angle Menu"
msgstr "Meni kotov"

#: phononnamespace.cpp:55
msgctxt "Phonon::|"
msgid "Notifications"
msgstr "Obvestila"

#: phononnamespace.cpp:57
msgctxt "Phonon::|"
msgid "Music"
msgstr "Glasba"

#: phononnamespace.cpp:59
msgctxt "Phonon::|"
msgid "Video"
msgstr "Video"

#: phononnamespace.cpp:61 phononnamespace.cpp:77
msgctxt "Phonon::|"
msgid "Communication"
msgstr "Komunikacije"

#: phononnamespace.cpp:63
msgctxt "Phonon::|"
msgid "Games"
msgstr "Igre"

#: phononnamespace.cpp:65
msgctxt "Phonon::|"
msgid "Accessibility"
msgstr "Dostopnost"

#: phononnamespace.cpp:79
msgctxt "Phonon::|"
msgid "Recording"
msgstr "Snemanje"

#: phononnamespace.cpp:81
msgctxt "Phonon::|"
msgid "Control"
msgstr "Nadzor"

#: pulsesupport.cpp:275 pulsesupport.cpp:286
msgctxt "QObject|"
msgid "PulseAudio Sound Server"
msgstr "Zvočni strežnik PulseAudio"

#: volumeslider.cpp:40 volumeslider.cpp:62 volumeslider.cpp:208
#: volumeslider.cpp:223
#, qt-format
msgctxt "Phonon::VolumeSlider|"
msgid "Volume: %1%"
msgstr "Glasnost: %1%"

#: volumeslider.cpp:43 volumeslider.cpp:65 volumeslider.cpp:123
#, qt-format
msgctxt "Phonon::VolumeSlider|"
msgid ""
"Use this slider to adjust the volume. The leftmost position is 0%, the "
"rightmost is %1%"
msgstr ""
"Uporabite ta drsnik, da nastavite glasnost. Skrajno levi položaj je 0%, "
"skrajno desni pa %1%"

#: volumeslider.cpp:203
msgctxt "Phonon::VolumeSlider|"
msgid "Muted"
msgstr "Utišan"

#, fuzzy
#~| msgid "Restart Application"
#~ msgctxt "Phonon::FactoryPrivate|"
#~ msgid "Restart Application"
#~ msgstr "Znova zaženi program"

#, fuzzy
#~| msgid ""
#~| "You changed the backend of the Phonon multimedia system.\n"
#~| "\n"
#~| "To apply this change you will need to restart '%1'."
#~ msgctxt "Phonon::FactoryPrivate|"
#~ msgid ""
#~ "You changed the backend of the Phonon multimedia system.\n"
#~ "\n"
#~ "To apply this change you will need to restart '%1'."
#~ msgstr ""
#~ "Spremenili ste zaledje predstavnostnega sistema Phonon.\n"
#~ "\n"
#~ "Da uveljavite to spremembo, boste morali znova zagnati '%1'."

#, fuzzy
#~| msgid "%0 by %1"
#~ msgctxt "QObject|"
#~ msgid "%0 by %1"
#~ msgstr "%0, avtor %1"
